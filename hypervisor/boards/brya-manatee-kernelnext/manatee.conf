# Copyright 2022 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

BOARD_IRQS=$(
  echo "edge:1"
  echo "edge:8"
  echo "level:14"
  echo "level:39"
  echo "edge:77"
  echo "level:100"

  # cros ec irq
  echo "level:101"
  echo "level:103"

  echo "level:117")

BOARD_GPES=$(
  # Typec wakeup
  echo "109"
  # EC gpe
  echo "110"
  # lid & cros ec wakeup gpe
  echo "81")

TBT_PCIE_HP_GPE="97"

BOARD_FIXED_EVENTS="powerbtn rtc"

BOARD_PMIO=$(
  # Super I/O
  echo -n "0x2e-0x2f,"
  echo -n "0x4e-0x4f,"
  # KBD
  echo -n "0x60,0x64,"
  # EC
  echo -n "0x62,0x66,"
  # RTC
  echo -n "0x70-0x77,"
  # Legacy DMA
  echo -n "0x87,"
  # CrOS EC
  echo -n "0x200,0x204,"
  echo -n "0x800-0x8ff,"
  # CrOS EC & EC opregion
  echo -n "0x900-0x9ff")

BOARD_MMIO=$(
  # INTC1055 (GPIO controller):
  echo -n "0xfd690000-0xfd69ffff,"
  echo -n "0xfd6a0000-0xfd6affff,"
  echo -n "0xfd6d0000-0xfd6dffff,"
  echo -n "0xfd6e0000-0xfd6effff,"
  # INTC1026 (intel scu):
  echo -n "0xfe000000-0xfe00ffff,"
  # intel pmc:
  echo -n "0xfbc10000-0xfbc115ff,"
  # camera i2c control:
  echo -n "0xfdad8024-0xfdad8027")

BOARD_MSR_READ_PASS=$(
  # MSR_PLATFORM_INFO
  echo "0xce"
  # MSR_PKG_CST_CONFIG_CONTROL
  echo "0xe2"
  # MSR_IA32_THERM_INTERRUPT
  echo "0x19b"
  # MSR_IA32_THERM_STATUS
  echo "0x19c"
  # MSR_IA32_TEMPERATURE_TARGET
  echo "0x1a2"
  # MSR_MISC_FEATURE_CONTROL
  echo "0x1a4"
  # MSR_MISC_PWR_MGMT
  echo "0x1aa"
  # MSR_TURBO_RATIO_LIMIT1
  echo "0x1ae"
  # MSR_IA32_PACKAGE_THERM_STATUS
  echo "0x1b1"
  # MSR_IA32_PACKAGE_THERM_INTERRUPT
  echo "0x1b2"
  # MSR_PKG_C3_RESIDENCY
  echo "0x3f8"
  # MSR_PKG_C6_RESIDENCY
  echo "0x3f9"
  # MSR_PKG_C7_RESIDENCY
  echo "0x3fa"
  # MSR_CORE_C3_RESIDENCY
  echo "0x3fc"
  # MSR_CORE_C6_RESIDENCY
  echo "0x3fd"
  # MSR_CORE_C7_RESIDENCY
  echo "0x3fe"
  # MSR_PKGC3_IRTL
  echo "0x60a"
  # MSR_PKGC6_IRTL
  echo "0x60b"
  # MSR_PKGC7_IRTL
  echo "0x60c"
  # MSR_PKG_C2_RESIDENCY
  echo "0x60d"
  # MSR_PKG_POWER_LIMIT
  echo "0x610"
  # MSR_PKG_PERF_STATUS
  echo "0x613"
  # MSR_PKG_POWER_INFO
  echo "0x614"
  # MSR_DRAM_POWER_LIMIT
  echo "0x618"
  # MSR_DRAM_PERF_STATUS
  echo "0x61b"
  # MSR_PKG_C8_RESIDENCY
  echo "0x630"
  # MSR_PKG_C9_RESIDENCY
  echo "0x631"
  # MSR_PKG_C10_RESIDENCY
  echo "0x632"
  # MSR_PKGC8_IRTL
  echo "0x633"
  # MSR_PKGC9_IRTL
  echo "0x634"
  # MSR_PKGC10_IRTL
  echo "0x635"
  # MSR_PP0_POWER_LIMIT
  echo "0x638"
  # MSR_PP0_POLICY
  echo "0x63a"
  # MSR_PP1_POWER_LIMIT
  echo "0x640"
  # MSR_PP1_POLICY
  echo "0x642"
  # MSR_CONFIG_TDP_NOMINAL
  echo "0x648"
  # MSR_CONFIG_TDP_LEVEL_1
  echo "0x649"
  # MSR_CONFIG_TDP_LEVEL_2
  echo "0x64A"
  # MSR_CONFIG_TDP_CONTROL
  echo "0x64B"
  # MSR_TURBO_ACTIVATION_RATIO
  echo "0x64C"
  # MSR_PKG_WEIGHTED_CORE_C0_RES
  echo "0x658"
  # MSR_PKG_ANY_CORE_C0_RES
  echo "0x659"
  # MSR_PKG_ANY_GFXE_C0_RES
  echo "0x65A"
  # MSR_PKG_BOTH_CORE_GFXE_C0_RES
  echo "0x65B"
  # MSR_CORE_C1_RES
  echo "0x660"
  # MSR_MODULE_C6_RES_MS
  echo "0x664"
  # MSR_RING_PERF_LIMIT_REASONS
  echo "0x6B1"
  # MSR_HWP_CAPABILITIES
  echo "0x771"
  # MSR_HWP_REQUEST_PKG
  echo "0x772")

BOARD_MSR_READ_WRITE_PASS=$(
  # MSR_IA32_PERF_CTL
  echo "0x199"
  # MSR_IA32_ENERGY_PERF_BIAS
  echo "0x1b0"
  # MSR_PM_ENABLE
  echo "0x770"
  # MSR_HWP_INTERRUPT
  echo "0x773"
  # MSR_HWP_REQUEST
  echo "0x774"
  # MSR_HWP_STATUS
  echo "0x777")

BOARD_MSR_READ_PASS_FILTER=$(
  # Energy stats
  # safe to skip KVM emulation and
  # pass through to guest

  # MSR_RAPL_POWER_UNIT
  echo "0x606"
  # MSR_PKG_ENERGY_STATUS
  echo "0x611"
  # MSR_DRAM_ENERGY_STATUS
  echo "0x619"
  # MSR_PP0_ENERGY_STATUS
  echo "0x639"
  # MSR_PP1_ENERGY_STATUS
  echo "0x641")
